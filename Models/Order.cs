﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoDash.Models
{
    public class Order
    {
        /// <summary>
        /// Gets or sets the customer name.
        /// </summary>
        public string CustomerName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the total price.
        /// </summary>
        public decimal TotalPrice { get; set; } = 0M;

        /// <summary>
        /// Gets or sets the referenced order details of this order.
        /// </summary>
        public List<OrderDetail> OrderDetails { get; set; } = new List<OrderDetail>();

        /// <summary>
        /// Gets or sets the ordered at.
        /// </summary>
        public DateTimeOffset OrderedAt { get; set; }

        /// <summary>
        /// Gets or sets the kicked at.
        /// </summary>
        public DateTimeOffset KickedAt { get; set; }
    }
}
