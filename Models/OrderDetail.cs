﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoDash.Models
{
    public class OrderDetail
    {
        /// <summary>
        /// Gets or sets the order detail ID.
        /// </summary>
        public Guid OrderDetailId { get; set; } = Guid.Empty;

        /// <summary>
        /// Gets or sets the food id.
        /// </summary>
        public int FoodId { get; set; } = 0;

        /// <summary>
        /// Gets or sets the food name.
        /// </summary>
        public string FoodName { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the food's price.
        /// </summary>
        public decimal Price { get; set; } = 0M;
    }
}
