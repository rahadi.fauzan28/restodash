﻿using RestoDash.Data;
using RestoDash.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoDash.Services
{    
    public class OrderService
    {       
        public void DisplayMenu()
        {
            var displayService = new DisplayService();
            Console.Clear();

            var isExitOption = false;
            while (isExitOption == false)
            {
                displayService.DisplayTable();
                DisplayMenuOptions();
                var input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        ServeCustomer();
                        break;
                    case "2":

                        break;
                    case "0":
                        isExitOption = true;
                        break;

                    default:
                        Console.WriteLine("Invalid option. Please choose a valid option.");
                        Console.ReadLine();
                        break;
                }

                Console.Clear();
            }
        }

        /// <summary>
        /// Display the menu options.
        /// </summary>
        public void DisplayMenuOptions()
        {
            var menuText = @"Accept Order Menu:
1. Serve customer
2. Kick customer
0. Exit
Please choose your choice [0..2]:";

            Console.WriteLine(menuText);
        }

        public void ServeCustomer()
        {
            var seat = new Seat();
            if (seat.OrderId == null)
            {
                var customerName = ValidateCustomerName();
                var foodId = ValidateFoodId();
            }
        }

        public string ValidateCustomerName()
        {
            var isValidCustomerName = false;
            var customerName = string.Empty;
            while (isValidCustomerName == false)
            {
                Console.WriteLine("Please input your name:");
                customerName = Console.ReadLine();

                if (string.IsNullOrEmpty(customerName) == true)
                {
                    Console.WriteLine("Customer name cannot be empty.");
                }
                else if (customerName.Length < 1)
                {
                    Console.WriteLine("Customer name must be more than 0 character.");
                }
                else
                {
                    isValidCustomerName = true;
                }
            }

            return customerName!;
        }

        public int ValidateFoodId()
        {
            var isValidFoodId = false;
            var foodId = 0;
            while (isValidFoodId == false)
            {
                Console.WriteLine("Please choose your Food ID:");
                var isFoodIdInt = int.TryParse(Console.ReadLine(), out var inputFoodId);
                var isExist = ValidateFoodIdExist(inputFoodId);

                if (isFoodIdInt == false)
                {
                    Console.WriteLine("Invalid Food ID. Please input a valid integer value.");
                }
                else
                {
                    if (isExist == false)
                    {
                        Console.WriteLine("Food id does not exist");
                    }
                    else
                    {
                        foodId = inputFoodId;
                        isValidFoodId = true;
                    }
                }
            }

            return foodId;
        }

        public bool ValidateFoodIdExist(int foodId)
        {
            foreach(var food in FoodMenu.Foods)
            {
                if(foodId == food.FoodId)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
