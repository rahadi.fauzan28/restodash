﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoDash.Services
{
    public class ViewOrderService
    {
        public void DisplayMenu()
        {
            Console.Clear();

            var isExitOption = false;
            while (isExitOption == false)
            {
                DisplayMenuOptions();
                var input = Console.ReadLine();

                switch (input)
                {
                    case "1":                        
                        break;
                    case "2":

                        break;
                    case "0":
                        isExitOption = true;
                        break;

                    default:
                        Console.WriteLine("Invalid option. Please choose a valid option.");
                        Console.ReadLine();
                        break;
                }

                Console.Clear();
            }
        }

        /// <summary>
        /// Display the menu options.
        /// </summary>
        public void DisplayMenuOptions()
        {
            var menuText = @"Options:
1. Search by customer name
2. See details
0. Exit
Please choose your choice [0..2]:";

            Console.WriteLine(menuText);
        }
    }
}
