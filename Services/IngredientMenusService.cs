﻿using RestoDash.Data;
using RestoDash.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoDash.Services
{
    /// <summary>
    /// Store various methods for handling ingredient menu data maintenance requests.
    /// </summary>
    public class IngredientMenusService
    {
        public void DisplayMenu()
        {
            Console.Clear();

            var isExitOption = false;
            while (isExitOption == false)
            {
                DisplayExistingIngredient();
                DisplayMenuOptions();
                var input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        AddNewIngredient();
                        break;
                    case "2":
                        EditIngredient();
                        break;
                    case "3":
                        DeleteIngredient();
                        break;
                    case "0":
                        isExitOption = true;
                        break;
                    default:
                        Console.WriteLine("Invalid option. Please choose a valid option.");
                        Console.ReadLine();
                        break;
                }

                Console.Clear();
            }
        }

        /// <summary>
        /// Display the menu options.
        /// </summary>
        public void DisplayMenuOptions()
        {
            var menuText = @"Maintain Ingredient Menu:
1. Add ingredient
2. Edit ingredient
3. Delete ingredient
0. Exit
Please choose your choice [0..3]:";

            Console.WriteLine(menuText);
        }

        /// <summary>
        /// Display the existing registered ingredient.
        /// </summary>
        public void DisplayExistingIngredient()
        {
            Console.WriteLine("Registered Ingredients:");
            var index = 1;
            Console.WriteLine("| No. | Name | Quantity | Ingredient ID |");

            if (IngredientMenu.Ingredients.Count > 0)
            {
                foreach (var ingredient in IngredientMenu.Ingredients)
                {
                    Console.WriteLine($"| {index} | {ingredient.Name} | { ingredient.Qty } | {ingredient.IngredientId} |");

                    index++;
                }
            }
            else
            {
                Console.WriteLine("No data available.");
            }

            Console.WriteLine("=================================");
        }

        /// <summary>
        /// Add new ingredient based on the user inputs.
        /// </summary>
        public void AddNewIngredient()
        {
            var ingredientName = ValidateIngredientName();
            var ingredientQty = ValidateIngredientQty();

            var latestIngredientId = IngredientMenu.Ingredients.Max(Q => Q.IngredientId);
            IngredientMenu.Ingredients.Add(new Ingredient
            {
                IngredientId = latestIngredientId + 1,
                Name = ingredientName,
                Qty = ingredientQty
            });
        }

        /// <summary>
        /// Validate the ingredient name input data.
        /// </summary>
        /// <returns></returns>
        public string ValidateIngredientName()
        {
            var isValidIngredientName = false;
            var ingredientName = string.Empty;
            while (isValidIngredientName == false)
            {
                Console.WriteLine("Please input your ingredient name:");
                ingredientName = Console.ReadLine();
                var isNameExists = ValidateNameExist(ingredientName);

                if (string.IsNullOrEmpty(ingredientName) == true)
                {
                    Console.WriteLine("Ingredient name cannot be empty.");
                }
                else if (ingredientName.Length < 4 || ingredientName.Length > 50)
                {
                    Console.WriteLine("Ingredient name minimum length must be [4-50] characters.");
                }
                else if (isNameExists == true)
                {
                    Console.WriteLine("Ingredient name already exist.");
                }
                else
                {
                    isValidIngredientName = true;
                }
            }

            return ingredientName!;
        }

        /// <summary>
        /// To check if the ingredient name already exist.
        /// </summary>
        public bool ValidateNameExist(string ingredientName)
        {
            foreach(var ingredient in IngredientMenu.Ingredients)
            {
                if(ingredientName == ingredient.Name)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Validate the ingredient quantity input data.
        /// </summary>
        public int ValidateIngredientQty()
        {
            var isQtyValid = false;
            var ingredientQty = 0;
            while (isQtyValid == false)
            {
                Console.WriteLine("Please input your ingredient quantity:"); 
                var isQtyInt = int.TryParse(Console.ReadLine(), out var inputQty);

                if (isQtyInt == false)
                {
                    Console.WriteLine("Invalid quantity input. Please input a valid integer value."); 
                }
                else
                {
                    if (inputQty < 0 || inputQty > 100)
                    {
                        Console.WriteLine("The quantity number must be between [0-100]");
                    }
                    else
                    {
                        ingredientQty = inputQty;
                        isQtyValid = true;
                    }
                }
            }
            return ingredientQty;
        }

        /// <summary>
        /// Edit the existing ingredient based on the user inputs.
        /// </summary>
        public void EditIngredient()
        {
            if (IngredientMenu.Ingredients.Count == 0)
            {
                DisplayContinueMessage(@"No data available.
Press any key to continue....");
                return;
            }

            var ingredientId = ValidateIngredientId();
            var ingredientName = ValidateIngredientName();
            var ingredientQty = ValidateEditIngredientQty(ingredientId);

            // Should be never null after we have validate the food ID.
            var ingredient = IngredientMenu.Ingredients
                .FirstOrDefault(Q => Q.IngredientId == ingredientId)!;

            ingredient.Name = ingredientName;
            ingredient.Qty = ingredientQty;
            
            DisplayContinueMessage(@"Edit success!
Press any key to continue....");
        }

        /// <summary>
        /// Delete the existing ingredient based on the user inputs.
        /// </summary>
        public void DeleteIngredient()
        {
            if (IngredientMenu.Ingredients.Count == 0)
            {
                DisplayContinueMessage(@"No data available.
Press any key to continue....");
                return;
            }

            var ingredientId = ValidateDeleteIngredientId();

            // Should be never null after we have validate the food ID.
            var ingredient = IngredientMenu.Ingredients
                .FirstOrDefault(Q => Q.IngredientId == ingredientId)!;

            Console.WriteLine($"Are you sure you want to delete [{ingredient.Name}]?(y/n)");
            var confirmOption = Console.ReadLine();

            if (string.IsNullOrEmpty(confirmOption) || confirmOption.ToLower() != "y")
            {
                DisplayContinueMessage(@"Cancelling deletion process....
Press any key to continue....");
                return;
            }

            IngredientMenu.Ingredients.Remove(ingredient);
            DisplayContinueMessage(@"Deletion success!
Press any key to continue....");
        }

        /// <summary>
        /// Validate the ingredient ID input data.
        /// </summary>
        /// <returns></returns>
        public int ValidateIngredientId()
        {
            var isValidIngredientId = false;
            var ingredientId = 0;
            while (isValidIngredientId == false)
            {
                Console.WriteLine("Please choose your ingredient ID:");
                var isIngredientIdInt = int.TryParse(Console.ReadLine(), out var inputIngredientId);

                if (isIngredientIdInt == false)
                {
                    Console.WriteLine("Invalid ingredient ID. Please input a valid integer value.");
                }
                else
                {
                    var isExistingIngredient = IngredientMenu.Ingredients
                        .Exists(Q => Q.IngredientId == inputIngredientId);

                    if (isExistingIngredient == false)
                    {
                        Console.WriteLine("Ingredient ID not found. Please input a valid ingredient ID.");
                    }
                    else
                    {
                        ingredientId = inputIngredientId;
                        isValidIngredientId = true;
                    }
                }
            }

            return ingredientId;
        }

        /// <summary>
        /// Validate the ingredient quantity input data for edit ingredient.
        /// </summary>
        public int ValidateEditIngredientQty(int ingredientId)
        {
            var ingredient = IngredientMenu.Ingredients
                .FirstOrDefault(Q => Q.IngredientId == ingredientId)!;
            var isQtyValid = false;
            var ingredientQty = ingredient.Qty;
            while (isQtyValid == false)
            {
                Console.WriteLine("Please input your ingredient quantity:");
                var isQtyInt = int.TryParse(Console.ReadLine(), out var inputQty);

                if (isQtyInt == false)
                {
                    Console.WriteLine("Invalid quantity input. Please input a valid integer value.");
                }
                else
                {
                    var temp = ingredientQty + inputQty;
                    if(temp < 0 || temp > 100)
                    {
                        Console.WriteLine("Total quantity must be between [0-100].");
                    }
                    else
                    {
                        ingredientQty = temp;
                        isQtyValid = true;
                    }
                }
            }
            return ingredientQty;
        }

        /// <summary>
        /// Validate the ingredient ID input data for delete ingredient.
        /// </summary>
        /// <returns></returns>
        public int ValidateDeleteIngredientId()
        {
            var isValidIngredientId = false;
            var ingredientId = 0;
            while (isValidIngredientId == false)
            {
                Console.WriteLine("Please choose your ingredient ID:");
                var isIngredientIdInt = int.TryParse(Console.ReadLine(), out var inputIngredientId);
                var isReferenced = ValidateIngredientReferenced(inputIngredientId);

                if (isIngredientIdInt == false)
                {
                    Console.WriteLine("Invalid ingredient ID. Please input a valid integer value.");
                }
                else
                {
                    if (isReferenced == true)
                    {
                        Console.WriteLine("Ingredient cannot be deleted because it is still being referenced.");
                    }
                    else
                    {
                        ingredientId = inputIngredientId;
                        isValidIngredientId = true;
                    }
                }
            }

            return ingredientId;
        }

        /// <summary>
        /// Validate the ingredient id is currently used to create food or not.
        /// </summary>
        public bool ValidateIngredientReferenced(int ingredientId)
        {
            foreach (var food in FoodMenu.Foods)
            {                
                foreach (var ingredient in food.IngredientRequirements)
                {
                    if (ingredientId == ingredient.IngredientId)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Display the continue prompt messages.
        /// </summary>
        /// <param name="message"></param>
        public void DisplayContinueMessage(string message)
        {
            Console.WriteLine(message);
            Console.ReadLine();
        }

    }
}
