﻿using RestoDash.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoDash.Data
{
    public static class OrderData
    {
        public static Dictionary<int, Order> Orders { get; set; } = new Dictionary<int, Order>();
    }
}
